var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');


// Static Server + watching sass/html files
gulp.task('serve', function () {
  browserSync.init({
    server: {
      baseDir: "./template/"
    }
  });
});



gulp.task('sass', function () {
  return gulp.src('./template/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({ errLogToConsole: true }))
    // .pipe(autoprefixer('last 2 versions', '> 1%', 'ie 9'))
    // .pipe(csso())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./template/css'))
    .pipe(browserSync.stream());

});


gulp.task('watch', function () {
  // gulp.watch("./*.html").on('change', browserSync.stream());
  gulp.watch("./template/*.html", browserSync.reload);
  gulp.watch('./template/sass/**/*.scss', ['sass']);
  // gulp.watch("./css/*.css", browserSync.reload);
  // gulp.watch("./css/*.css").on('change', browserSync.stream());
  // gulp.watch("./js/*.js", browserSync.reload);
});

gulp.task('default', ['serve', 'sass', 'watch']);
